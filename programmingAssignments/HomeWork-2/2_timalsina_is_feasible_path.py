#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 23:40:06 2018

@author: umesh
"""

def is_feasible_path(app_path):
    """Returns true if the ride is exactly of 10 minutes and 
       gets the rider back to his/her original position
    
    """
    nc, wc, ec, sc = 0, 0, 0, 0
    if not isinstance(app_path, list):
        raise ValueError("Expected a list but got ", type(app_path))
        
    if len(app_path) != 10:
        return 0
    for block in app_path:
        if block == 'N':
            nc = nc+1
        elif block == 'E':
            ec = ec+1
        elif block == 'W':
            wc = wc+1
        elif block == 'S':
            sc = sc+1
    return (nc == sc) and (ec == wc)

if __name__ == "__main__":
    print(is_feasible_path(['E', 'N', 'E',
                  'N', 'S', 'N', 
                  'W', 'S','S', 
                  'S'
                 ]))