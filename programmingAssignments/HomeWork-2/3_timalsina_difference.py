#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  1 00:12:46 2018

@author: umesh
"""

def difference(num):
    """Returns the difference between the square of sum of numbers [1, num]
    and sum of square if numbers [1, num]
    """
    sq_sum, sum_sq = 0, 0
    for i in range(1, num+1):
        sq_sum += i**2
        sum_sq += i
    sum_sq = sum_sq**2
    return sum_sq-sq_sum

if __name__ == '__main__':
    print(difference(10))
        