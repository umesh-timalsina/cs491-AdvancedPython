#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 22:00:00 2018

@author: umesh
"""

def digits_squared(num):
    """Returns square of every digits of num, an integer value
    
    """
    if not isinstance(num, int):
        raise ValueError('parameter should be of integer type')
    d_squared = []
    for d in str(num):
        d_squared.append(str(int(d)**2))
    d_sq = ''.join(d_squared)
    return int(d_sq)

if __name__ == "__main__":
    num = input("Enter an integer")
    print(digits_squared(num))

    