#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  1 00:25:06 2018

@author: umesh
"""

def distinct_count(x, y):
    """Calculates the number of distinct terms for the sequence h^t such that
        2 <= h <= x and 2 <= t <= y.
        
    """
    draft_list = []
    for i in range(2, x+1):
        for j in range(2, y+1):
            draft_list.append(i**j)
    draft_list.sort()
    return len(list(set(draft_list)))

if __name__ == "__main__":
    print(distinct_count(5, 5))