#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 15 18:42:24 2018

@author: umesh
"""

# Mandatory Imports
import requests
import re
from bs4 import BeautifulSoup

class FacultyDirectory():
    '''
    A class that scrapes data out of the SIU's CS URL.
    '''
    def __init__(self):
        self.url = "http://cs.siu.edu/faculty-staff/continuing_faculty.php"
         
    def connect(self):
        r = requests.get(self.url)
        if r.status_code != 200:
            raise ConnectionError
        self.text = r.text
    
    def parse(self):
        soup = BeautifulSoup(self.text, 'html.parser')
        faculties = soup.find_all('div', class_='people-wrapper')
        for faculty in faculties:
            fac_soup = BeautifulSoup(str(faculty), 'html.parser')
            fac_list = fac_soup.get_text().split('\n') 
            fac_list  = [c for c in fac_list if c != '']
            # Name and positions are quite easy
            print('Name: ', fac_list[0])
            print('Position: ', fac_list[1])
            # Email filter out Nick, Garth and Diane
            if fac_list[2].find('Email') != -1:
                print(re.search(r'Phone:.*[0-9]', fac_list[2]).group(0))
                print(re.search(r'Office:.*P', fac_list[2]).group(0)[0:-1])
                print(re.search(r'Email:.*edu', fac_list[2]).group(0))
            # All others
            else:
                print(fac_list[3], fac_list[2], sep='\n')
                if(len(fac_list) > 4):
                    print(fac_list[4])
        
            # Printing Homepage for every professor
            if fac_list[-1].find('Homepage:') != -1:
                print(re.search(r'Homepage:.*', fac_list[-1]).group(0))
            else:
                print('Homepage: N/A')
            print('****************************************************')

if __name__ == "__main__":
    fd = FacultyDirectory() # Create a new object
    fd.connect()
    fd.parse()

    