#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 18:14:19 2018

@author: umesh
"""

def book(input_list):
    """
    A function that takes list as an input and prints some text accordingly
    """
    if len(input_list)==0:
        print("no one has read this")
    elif len(input_list)==1:
        print(input_list[0], "has read this")
    elif len(input_list) == 2:
        print('{0} and {1} have read this'.format(input_list[0], input_list[1]))
    elif len(input_list) == 3:
        print('{0}, {1} and {2} have read this'.
              format(input_list[0], input_list[1], input_list[2]))
    else:
        print('{0}, {1} and {2} others have read this'.
              format(input_list[0], input_list[1], len(input_list)-2))

if __name__ == "__main__":
    book(['ram', 'hari', 'geeta', 'seeta', 'neeta'])