#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 18:43:53 2018

@author: umesh
"""

def num2list(num):
    '''
    Return a list of digits of a number that is provided as input
    param: num is the input number
    returns: list containing digits of the number
    '''
    list_out = []
    rem = 0
    while num > 0:
        rem = num % 10
        num = int(num / 10)
        list_out.append(rem)
    return list(reversed(list_out))


def encrypt(message, key):
    '''
    takes a message string and key number and returns an array of integers
    representing the encoded message
    param1: message is the mesage to be encoded
    param2: key is the key to be used
    returns: an array(list) of integers as the encoded message
    '''
    key_digits=num2list(key)
    encrypted_output=[]
    dict_main = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6, 'g': 7,
                 'h': 8, 'i': 9, 'j': 10, 'k': 11, 'l': 12, 'm': 13, 'n': 14, 
                 'o': 15, 'p': 16, 'q': 17, 'r': 18, 's': 19,'t': 20, 'u': 21,
                 'v': 22, 'w': 23, 'x': 24, 'y': 25, 'z': 26}
    msg_letters = list(message)
    key_index=0
    for i in range(0, len(msg_letters)):
        encrypted_output.append((dict_main[msg_letters[i]] 
        + key_digits[key_index])%26) # Modulo for roll-overs
        key_index = key_index+1 
        if key_index == len(key_digits):
            key_index = 0
    return encrypted_output
    
if __name__ == "__main__":
    op1 = encrypt("zebra", 2824)
    op2 = encrypt("applepie", 1824)
    print(op1)