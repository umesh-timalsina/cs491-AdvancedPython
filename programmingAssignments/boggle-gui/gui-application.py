#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May  6 20:50:39 2018

@author: umesh
"""

import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from randomizer import SixteenDices
from input_processor import InputProcessor
from main import Main, match_dictionary

class BoggleGUI(QMainWindow):
    EXIT_CODE_REBOOT = -123
    def __init__(self):
        super().__init__()
        self.dices = SixteenDices()
        self.dices_list = self.dices.get_dice_grid()
        self.logic_main = Main()
        self.input_words_list = list()
        self.input_processor = InputProcessor()
        self.initUI()
        
    def initUI(self):
        self.centerWidget = QWidget()
        self.boggleTextWidget = QWidget(self.centerWidget)
        self.wordEnterWidget = QWidget(self.centerWidget)
        self.wordListWidget = QWidget(self.centerWidget)
        self.centralWidgetLayout = QVBoxLayout()
        self.textGridLayout = QGridLayout()
        self.wordEnterLayout = QHBoxLayout()
        self.wordListLayout = QHBoxLayout()
        self.setCentralWidget(self.centerWidget)
        # Work on the text grid 
        self.boggleTextWidget.setLayout(self.textGridLayout)
        for i in range(0, 4):
            for j in range(0, 4):
                button = QPushButton(self.dices_list[i][j])
                self.textGridLayout.addWidget(button, i, j)
                
        # Work on word entryprint('I am here')
        self.wordEnterWidget.setLayout(self.wordEnterLayout)
        self.lineEdit = QLineEdit()
        self.addWordBtn = QPushButton('Add Word')
        self.wordEnterLayout.addWidget(self.lineEdit)   
        self.wordEnterLayout.addWidget(self.addWordBtn)
        input
        # Work on everything related to word list
        self.wordListWidget.setLayout(self.wordListLayout)
        self.wordList = QTextEdit()
        self.scoreBtn = QPushButton('Score')
        self.wordListLayout.addWidget(self.wordList)
        self.wordListLayout.addWidget(self.scoreBtn)
        
        # Add Button listeners
        self.addWordBtn.clicked.connect(self.addWordBtn_clicked)
        self.scoreBtn.clicked.connect(self.scoreBtn_clicked)
        
        # Add Menu 
        menubar = self.menuBar()
        gameMenu = menubar.addMenu('Game')
        newGame = QAction('New Game', self)
        gameMenu.addAction(newGame)
        newGame.triggered.connect(self.new_game)

        
        #Add everything to the central widget
        self.centerWidget.setLayout(self.centralWidgetLayout)
        self.centralWidgetLayout.addWidget(self.boggleTextWidget)
        self.centralWidgetLayout.addWidget(self.wordEnterWidget)
        self.centralWidgetLayout.addWidget(self.wordListWidget)
        
        # Display the GUI
        self.setGeometry(300, 300, 500, 500)
        self.setWindowTitle('Boggle Game')
        self.setWindowIcon(QIcon('./images/boggle.jpeg'))
        self.show()
    
    # Start a new Game
    def new_game(self):
        self.dices = None
        self.dices_list = None
        self.input_processor = None
        self.logic_main = None
        qApp.exit(BoggleGUI.EXIT_CODE_REBOOT)
        return
        
    def addWordBtn_clicked(self):
        word_entered = self.lineEdit.text()
        if word_entered:
            self.input_words_list.append(word_entered.strip().upper())
            self.lineEdit.clear()
            self.input_words_list = list(set(self.input_words_list))
            word_list_display = '\n'.join(self.input_words_list)
            self.wordList.setText(word_list_display)
        return
    
    def scoreBtn_clicked(self):
        self.input_processor.set_input_list(self.input_words_list)
        self.logic_main.setInputProcessor(self.input_processor)
        self.logic_main.setSixteenDices(self.dices)
        match_dictionary()
        self.logic_main.word_combination()
        reply = QMessageBox.question(self, 'Game Over',
                                     'Score: {0} \n Play Again?'.format(
                                     str(self.logic_main.getScore())),
                                     QMessageBox.Yes | QMessageBox.No, 
                                     QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.new_game()
        else:
            sys.exit(0)
        return
    
if __name__ == "__main__":
    currentExitCode = BoggleGUI.EXIT_CODE_REBOOT
    while currentExitCode == BoggleGUI.EXIT_CODE_REBOOT:
        app = QApplication(sys.argv)
        gui = BoggleGUI()
        currentExitCode = app.exec_()
        app = None
    