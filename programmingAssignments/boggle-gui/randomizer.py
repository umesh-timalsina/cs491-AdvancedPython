#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  2 20:07:50 2018

@author: umesh
"""

# Mandatory Imports
import random

""" This class is to initialize die faces
"""

class SixteenDices:
    """ Initialize with initial dictionary
    """
    def __init__(self):
        self.dices = { 1  : ['A', 'E', 'A', 'N', 'E', 'G'],  
                       2  : ['A', 'H', 'S', 'P', 'C', 'O'],
                       3  : ['A', 'S', 'P', 'F', 'F', 'K'],
                       4  : ['O', 'B', 'J', 'O', 'A', 'B'],
                       5  : ['I', 'O', 'T', 'M', 'U', 'C'],
                       6  : ['R', 'Y', 'V', 'D', 'E', 'L'],
                       7  : ['L', 'R', 'E', 'I', 'X', 'D'],
                       8  : ['E', 'I', 'U', 'N', 'E', 'S'],
                       9  : ['W', 'N', 'G', 'E', 'E', 'H'],
                       10 : ['L', 'N', 'H', 'N', 'R', 'Z'],
                       11 : ['T', 'S', 'T', 'I', 'Y', 'D'],
                       12 : ['O', 'W', 'T', 'O', 'A', 'T'],
                       13 : ['E', 'R', 'T', 'T', 'Y', 'L'],
                       14 : ['T', 'O', 'E', 'S', 'S', 'I'],
                       15 : ['T', 'E', 'R', 'W', 'H', 'V'],
                       16 : ['N', 'U', 'I', 'H', 'M', 'Qu'],
                }
        self.dice_grid = [[]]
        self.display_dices()
    
    """ Display the dices in predefined order
    """
    def display_dices(self):
        temp_list = []
        for val in self.dices.keys():
            value = self.dices[val][random.randint(0, 5)]
            print('[', value, ']', sep = " ", 
                 end = "" if (val%4) != 0 else '\n')
            temp_list.append(value)
            if val%4 == 0:
                self.dice_grid.append(temp_list)
                temp_list = []
        self.dice_grid = self.dice_grid[1:]
    
    def get_dice_grid(self):
        return self.dice_grid