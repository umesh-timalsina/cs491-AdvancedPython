#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  8 21:40:03 2018

@author: umesh
"""
# Mandatory imports
from functools import reduce
import operator

def difference(num):
    """Returns the difference between the square of sum of numbers [1, num]
        and sum of square if numbers [1, num]
    """
    sum_sq = reduce(operator.add, 
                    map(lambda x : x**2, range(0, num+1)))  # Find squares, add them
    sq_sum = (reduce(operator.add,
                    [x for x in range(1, num+1)]))**2  # Add numbers, find squares
    return (sq_sum-sum_sq)
    
if __name__ == '__main__':
    print(difference(10))
