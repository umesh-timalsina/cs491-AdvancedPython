#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 22 19:13:42 2018

@author: umesh
"""

# Mandatory Imports
import re

class PhoneNumberPrinter():
    """A class that matches phone numbers out of the given text
    """
    
    def __init__(self, loc):
        self.file_loc = str(loc)
        self.ph_regex = r'([1-9][0-9]{2}-[0-9]{4})'  
    
    def match(self):
        try:
            with open(self.file_loc, 'r') as fp:
                content = fp.read()
                regex = re.compile(self.ph_regex)
                m = regex.findall(content)
                print(*m, sep='\n')
        except FileNotFoundError:
            print('Oops File not found')
            

if __name__ == "__main__":
    phoneNumberPrinter = PhoneNumberPrinter('./files/contacts.txt')
    phoneNumberPrinter.match()
        
        