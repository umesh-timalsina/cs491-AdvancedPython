#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 22 20:50:26 2018

@author: umesh
"""

# Mandatory imports
import re

class EmailMatcher():
    """ A class written to match valid email addresses in the file.
    """
    
    def __init__(self, loc):
        self.file_loc = str(loc)
        self.email_regex = r'(?P<email>[a-zA-Z][a-zA-Z0-9]+' \
                                '@[a-zA-Z]+[.][a-zA-Z]+)' 
                        
    
    def match(self):
        try:
            with open(self.file_loc, 'r') as fp:
                content = fp.read()
                regex = re.compile(self.email_regex)
                m = regex.findall(content)
                print(*m, sep='\n')
        except FileNotFoundError:
            print('oops File not found')
            

if __name__ == "__main__":
    email_matcher = EmailMatcher('./files/emails.txt')
    email_matcher.match()