#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Umesh Timalsina (SIU854422010)
"""

# Mandatory Imports
import sys
import time
import logging

# In case of recovery needed
std_out = sys.__stdout__
def log(file_name = None):
    '''
        A generic decorator written for logging function calls
        
        This decorator is written for handling and logging the summary of 
        function calls in python. Basically, any function call can be appended
        this decorator using syntatic sugar convenstions for this decorator.
        
        Parameters
       --------------
        file_name = None : str
           The name of the file in which the output is to be put. If left empty
           the output of the method is logged to stdout. Otherwise, a new file
           is created and the standard sys.stdout is piped to the file 
           descriptor obtained by open.
        
        Returns
       ----------
           a wrapper function for logging everything
        
    '''
    def log_decorator(function):
        def wrapper(*args, **kwargs):
            fp = None
            if file_name is not None:
                try:
                    fp = open(file_name, 'a')
                    sys.stdout = fp
                except IOError:
                    pass
            # Name of the logger
            logger = logging.getLogger('logger' + function.__name__)
            logger.setLevel(logging.DEBUG)  #  Set the level to DEBUG
            ch = logging.StreamHandler(sys.stdout)
            ch.setLevel(logging.DEBUG)  #  Stream handler to Debug
            formatter = logging.Formatter('%(message)s')
            ch.setFormatter(formatter)
            logger.addHandler(ch) # Add hander 
            logger.debug('******************************' 
                         +'*******************************')
            logger.debug('Calling function {}'
                             .format(function.__name__))
            if args is not None:
                logger.debug('Arguments:')
                for arg in args:
                    logger.debug('\t - ' + str(arg) 
                                         + ' of type ' 
                                         + type(arg).__name__)
            else:
                logger.debug('No Arguments')
        
            logger.debug('Output: ')
            start = time.time()
            retval = function(*args, **kwargs)
            end = time.time()
            logger.debug('Execution time: {:f} s.'.format(end-start))
            if retval is None:
                logger.debug('No return value.')
            else:
                logger.debug('Return value: {} of type {}'.
                                 format(str(retval), 
                                    type(retval).__name__))
            x = logger.handlers.copy()
            for i in x:
                logger.removeHandler(i)  # Remove all handlers
                i.flush()
                i.close()
            if fp is not None:
                fp.close()
                sys.stdout = sys.__stdout__  # Restore stdout
        return wrapper
    return log_decorator
        
        
        
@log('log.txt')
def factorial(*num_list):
    results = []
    print('Hello World')
    for number in num_list:
        res = number
        for i in range(number-1, 0, -1):
            res = i*res
        results.append(res)
    return results

@log()
def print_hello():
    print("Hello!")


@log("logger.txt")
def gcd(a, b):
    print("The GCD of", a, "and", b, "is ", end="")
    while a!=b:
        if a > b:
            a -= b 
        else:
            b -= a
        print(abs(a))
    return abs(a)
    
if __name__ == "__main__":
    factorial(2, 3, 4)
    print_hello()
    gcd(3, 4)
