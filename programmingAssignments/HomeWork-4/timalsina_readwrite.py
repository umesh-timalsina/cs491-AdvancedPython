#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 15 23:13:20 2018

@author: umesh
"""

def read_write():
    """
        Reads and writes selected texts to the file
        as directed by the question.
    """
    try:
        fp=open('file.txt', 'w+')
    except IOError:
        print('I/O Error: Trouble opening file')
    else:   # If no exception raised, do work
        fp.writelines(item for item in
                      ["This is line {}\n".format(x) 
                        for x in range(1, 4)])  
        fp.seek(0, 0)  # Return to beginning
        print(fp.readline(), fp.readline(), 
                      sep="", end="")
        third_line_pos = fp.seek(0, 1)  # Record the current position
        fp.seek(0, 2)  # Go to the end of file
        fp.write("This is the fourth line\n")
        fp.seek(third_line_pos, 0)  # Go to the third line
        print(fp.read())            # Print the rest of the file
        fp.close()
        
if __name__ == '__main__':
    read_write()
        
