#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 23 16:09:43 2018

@author: umesh
"""
def fibonacci_print():
    MAX_VALUE=4000000
    t0=1
    t1=2
    sum = t1
    current_term = t1
    while current_term<MAX_VALUE:
        current_term = t0+t1
        t0=t1
        t1=current_term
        if current_term%2==0:
            sum+=current_term
    return sum
    
if __name__ == "__main__":
    print(fibonacci_print())