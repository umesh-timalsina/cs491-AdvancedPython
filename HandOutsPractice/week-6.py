#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 16:11:50 2018

@author: umesh
"""

def echo(message):
    print(message)
    
    
echo('printing')

x = echo
x('printing again')

def indirect(func, arg):
    func(arg)

def make(label):
    def echo(message):
        print(label + ':' + message)
    return echo

    

indirect(echo, "Argument Calls")

schedule = [(echo, 'spam!'), (echo, 'Ham!')]
for (func, arg) in schedule:
    func(arg)

F = make('spam')
F('Ham')
    