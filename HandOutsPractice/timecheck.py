#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  2 18:10:06 2018

@author: umesh
"""

import time

def timer():
    s = time.time()
    time.sleep(5)
    e = time.time()
    print (e-s)

def cpu_timer():
    s = time.clock()
    time.sleep(5)
    e = time.clock()
    print(e-s)