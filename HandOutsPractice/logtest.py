import logging

logging.basicConfig(filename='example.log', level=logging.DEBUG)
logging.critical("Immenent fatal error")
logging.error("Couldnot perform some function but running")
logging.warning("Disk Space low")
logging.info("Everything seems to be working ok..")
logging.debug("Here is some info that might be useful to debug with..")

