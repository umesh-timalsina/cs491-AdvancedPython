#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 20:03:00 2018

@author: umesh
"""

import math 
def accepts(*arg_types):
    def arg_check(func):
        def new_func(*args):
            for arg, arg_type in zip(args, arg_types):
                if type(arg) != arg_type:
                    print("Argument", arg, 'is not of type', arg_type)
                    break
                else:
                    func(*args)
        return new_func
    return arg_check
    
@accepts(complex)
def complex_magnitude(z):
    print(math.sqrt(z.real**2 + z.imag**2))

print(complex_magnitude())