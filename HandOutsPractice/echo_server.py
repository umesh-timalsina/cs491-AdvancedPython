#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr  1 10:49:58 2018

@author: umesh
"""

import socketserver

class MyTCPSocketHandler(socketserver.BaseRequestHandler):
    """
    The Request Handler class for our server
    
    It is instantiated once per connection to the server, and must 
    override the handle() method to implement communication to the 
    client
    
    """
    
    def handle(self):
        self.data = self.request.recv(1024).strip()
        print("{} wrote: ".format(self.client_address[0]))
        print(self.data)
        self.request.sendall(self.data.upper())
        

if __name__ == "__main__":
    HOST, PORT = "localhost", 9999
    
    server = socketserver.TCPServer((HOST, PORT), MyTCPSocketHandler)
    
    server.serve_forever()