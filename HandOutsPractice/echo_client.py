#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr  1 10:57:00 2018

@author: umesh
"""

import socket, sys

HOST, PORT = "localhost", 9999

data = " ".join(sys.argv[1:])

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    sock.connect((HOST, PORT))
    sock.sendall(bytes(data + "\n", 'utf8'))
    
    received = str(sock.recv(1024))
    
finally:
    sock.close()
    
print("sent:  {}".format(data))
print("received:   {}".format(str(received)))