#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 21:31:44 2018

@author: umesh
"""

''' Module lambdas.py'''

g = lambda x: x**2
print(g(8))

h = (lambda a = 'fee', b = 'wee', c = 'gee' : (a+b+c))
print(h(a='wee'))

lower = (lambda x, y: x if x < y else y)
print(lower('bb', 'aa'))