#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 20:26:00 2018

@author: umesh
"""
''' Module bar.py '''
print("Hello from bar's top level")

def print_hello():
    print("Hello from bar!!")

if __name__ == "__main__":
    print("Bar's name is main")