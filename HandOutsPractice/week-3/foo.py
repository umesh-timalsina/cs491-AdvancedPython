#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 20:27:52 2018

@author: umesh
"""

''' Module foo.py '''

import bar
print("Hi from foo's top level")
if __name__ == "__main__":
    print("Foo's __name__ is __main__")
    bar.print_hello()